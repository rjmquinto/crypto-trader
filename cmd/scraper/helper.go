package main

import (
	"strconv"
	"time"

	"gitlab.com/rjmquinto/crypto-trader/pkg/candlesdb"
	"gitlab.com/rjmquinto/crypto-trader/pkg/cryptocom"
)

func stringToDuration(s string) time.Duration {
	c := s[len(s)-1]
	v, err := strconv.Atoi(s[:len(s)-1])
	val := time.Duration(v)

	if err != nil {
		return -1
	}
	if c == 'm' {
		return time.Minute * val
	} else if c == 'h' {
		return time.Hour * val
	} else if c == 'D' {
		return time.Hour * val * 24
	} else if c == 'M' {
		return time.Hour * val * 24 * 29
	} else {
		return -1
	}
}

func candlestickJSONToCandles(csj cryptocom.CandlestickJSON) []candlesdb.Candle {
	candles := make([]candlesdb.Candle, len(csj.Result.Data))
	for i, c := range csj.Result.Data {
		candles[i] = candlesdb.Candle{
			InstrumentName: csj.Result.InstrumentName,
			Interval:       csj.Result.Interval,
			Time:           c.Time,
			Open:           c.Open,
			High:           c.High,
			Low:            c.Low,
			Close:          c.Close,
			Volume:         c.Volume,
		}
	}

	return candles
}
