package main

import (
	"log"
	"time"

	"gitlab.com/rjmquinto/crypto-trader/pkg/candlesdb"
	"gitlab.com/rjmquinto/crypto-trader/pkg/cryptocom"
)

func main() {
	duration := []string{"1m", "5m", "15m", "30m", "1h", "4h", "6h", "12h", "1D", "7D", "14D", "1M"}

	for _, d := range duration {
		go func(d string) {
			scrapeCyclic(d)
			dur := stringToDuration(d)
			for range time.Tick(dur) {
				scrapeCyclic(d)
			}
		}(d)
	}

	select {}
}

func scrapeCyclic(timeframe string) {
	instruments, err := cryptocom.GetInstruments()
	if err != nil {
		log.Println(err)
		return
	}

	for _, inst := range instruments.Result.Instruments {
		csj, err := cryptocom.GetCandlestick(inst.InstrumentName, timeframe)
		if err != nil {
			log.Println(err)
			return
		}

		candles := candlestickJSONToCandles(csj)

		if err := candlesdb.InsertCandlesticks(candles); err != nil {
			log.Println(err)
			return
		}
	}
}
