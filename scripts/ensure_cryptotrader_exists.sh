#!/bin/sh

adduser --system cryptotrader --quiet
sudo -u postgres psql -c "CREATE USER cryptotrader" > /dev/null 2>&1
sudo -u postgres psql -c "CREATE DATABASE cryptotrader" > /dev/null 2>&1
sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE cryptotrader TO cryptotrader" > /dev/null 2>&1
