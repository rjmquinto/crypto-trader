package candlesdb

import (
	"log"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

var db *gorm.DB

func init() {
	dsn := "user=cryptotrader dbname=cryptotrader port=5432 TimeZone=Asia/Manila"
	ret, err := gorm.Open(postgres.Open(dsn), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Error),
	})

	if err != nil {
		log.Fatalln(err)
	}
	db = ret

	db.AutoMigrate(&Candle{})
}
