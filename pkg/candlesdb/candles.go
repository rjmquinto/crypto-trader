package candlesdb

import (
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

//Candle is the DB model struct for the candles
type Candle struct {
	gorm.Model
	InstrumentName string `gorm:"index:main,priority:1;uniqueIndex:time,priority:1" json:"instrument_name"`
	Interval       string `gorm:"index:main,priority:2;uniqueIndex:time,priority:2"`
	Time           int64  `gorm:"uniqueIndex:time,priority:3"`
	Open           float64
	High           float64
	Low            float64
	Close          float64
	Volume         float64
}

//InsertCandlesticks inserts multiple candlesticks to DB
func InsertCandlesticks(candles []Candle) error {
	if result := db.Clauses(clause.OnConflict{DoNothing: true}).
		Create(&candles); result.Error != nil {
		return result.Error
	}

	return nil
}

//SelectCandlesticks returns candlesticks for a particular instrument and timeframe
//if time>0, then candlesticks with time < t will be chosen
//if k>0, then up to k candlesticks will be returned. The default is 300
func SelectCandlesticks(instrumentName string, timeframe string, time uint, k int) ([]Candle, error) {
	if k == 0 {
		k = 300
	}

	var candles []Candle
	if time == 0 {
		if result := db.Limit(k).
			Where("instrument_name = ? AND interval = ?", instrumentName, timeframe).
			Order("time desc").
			Find(&candles); result.Error != nil {
			return nil, result.Error
		}
	} else {
		if result := db.Limit(k).
			Where("instrument_name = ? AND interval = ? AND time <= ?", instrumentName, timeframe, time).
			Order("time desc").
			Find(&candles); result.Error != nil {
			return nil, result.Error
		}
	}
	return candles, nil
}
