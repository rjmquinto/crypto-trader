package cryptocom

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"time"
)

func apiGetAndStore(url string, ret interface{}) error {
	resp, err := http.Get(url)
	if err != nil {
		return err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	if err := json.Unmarshal(body, ret); err != nil {
		return err
	}

	return nil
}

func apiPostAndStore(url string, reqBody map[string]interface{}, ret interface{}) error {
	req, err := json.Marshal(reqBody)
	if err != nil {
		return err
	}

	resp, err := http.Post(url, "application/json; charset=UTF-8", strings.NewReader(string(req)))
	if err != nil {
		return err
	}

	defer resp.Body.Close()
	data, err := ioutil.ReadAll(resp.Body)

	if err := json.Unmarshal(data, ret); err != nil {
		return err
	}

	return nil
}

func authorize(body map[string]interface{}) error {
	if _, exists := body["method"]; !exists {
		return fmt.Errorf("Key 'method' is missing")
	}

	id := ""
	if v, exists := body["id"]; exists {
		id = strconv.Itoa(v.(int))
	}

	nonce := time.Now().UnixNano() / 1000000

	paramString := ""
	if v, exists := body["params"]; exists {
		val := v.(map[string]interface{})
		keys := make([]string, len(val))
		i := 0
		for k := range val {
			keys[i] = k
			i++
		}

		sort.Strings(keys)

		for _, k := range keys {
			paramString = paramString + (k + fmt.Sprintf("%v", val[k]))
		}
	}

	sigPayload := body["method"].(string) + id + apiKey + paramString + strconv.FormatInt(nonce, 10)

	hash := hmac.New(sha256.New, []byte(apiSecret))
	if _, err := hash.Write([]byte(sigPayload)); err != nil {
		return err
	}

	body["nonce"] = nonce
	body["api_key"] = apiKey
	body["sig"] = hex.EncodeToString(hash.Sum(nil))
	return nil
}
