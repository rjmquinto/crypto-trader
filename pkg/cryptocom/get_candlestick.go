package cryptocom

import (
	"fmt"
)

//GetCandlestick "Retrieves candlesticks (k-line data history) over a given period for an instrument (e.g. BTC_USDT)"
func GetCandlestick(instrumentName string, timeframe string) (CandlestickJSON, error) {
	method := "public/get-candlestick"
	params := fmt.Sprintf("instrument_name=%v&timeframe=%v", instrumentName, timeframe)
	var ret CandlestickJSON
	if err := apiGetAndStore(baseURL+method+"?"+params, &ret); err != nil {
		return CandlestickJSON{}, err
	}

	return ret, nil
}

//CandlestickJSON response struct for
type CandlestickJSON struct {
	returnMeta
	Result struct {
		InstrumentName string `json:"instrument_name"`
		Interval       string
		Data           []Candlestick
	}
}

//Candlestick struct
type Candlestick struct {
	Time   int64   `json:"t"`
	Open   float64 `json:"o"`
	High   float64 `json:"h"`
	Low    float64 `json:"l"`
	Close  float64 `json:"c"`
	Volume float64 `json:"v"`
}
