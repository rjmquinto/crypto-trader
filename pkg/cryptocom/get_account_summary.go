package cryptocom

//GetAccountSummary "Returns the account balance of a user for a particular token"
//If currency is empty, all tokens are returned
func GetAccountSummary(currency string) (AccountSummaryJSON, error) {
	postBody := map[string]interface{}{
		"id":     1,
		"method": "private/get-account-summary",
		"params": map[string]interface{}{},
	}
	if currency != "" {
		postBody["params"].(map[string]interface{})["currency"] = currency
	}

	authorize(postBody)

	var ret AccountSummaryJSON
	if err := apiPostAndStore(baseURL+postBody["method"].(string), postBody, &ret); err != nil {
		return AccountSummaryJSON{}, nil
	}

	return ret, nil
}

//AccountSummaryJSON response struct for GetAccountSummary
type AccountSummaryJSON struct {
	returnMeta
	Result struct {
		Accounts []Account
	}
}

//Account struct for tokens
type Account struct {
	Balance   float64
	Available float64
	Order     float64
	Stake     float64
	Currency  string
}
