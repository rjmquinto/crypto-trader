package cryptocom

import (
	"fmt"
)

//GetBook "Fetches the public order book for a particular instrument and depth"
//`depth` can be up to 150. If depth is 0, the default value is provided.
func GetBook(instrumentName string, depth int) (BookJSON, error) {
	method := "public/get-instruments"
	params := fmt.Sprintf("instrument_name=%v&depth=%v", instrumentName, depth)
	var recv bookInterm
	if err := apiGetAndStore(baseURL+method+"?"+params, &recv); err != nil {
		return BookJSON{}, err
	}

	ret := BookJSON{}
	ret.ID = recv.ID
	ret.Method = recv.Method
	ret.Code = recv.Code

	ret.Result.Bids = make([]Offer, len(recv.Result.Bids))
	for i, b := range recv.Result.Bids {
		ret.Result.Bids[i] = Offer{
			Price:    b[0],
			Quantity: b[1],
			NumOrder: b[2],
		}
	}

	ret.Result.Asks = make([]Offer, len(recv.Result.Asks))
	for i, b := range recv.Result.Asks {
		ret.Result.Asks[i] = Offer{
			Price:    b[0],
			Quantity: b[1],
			NumOrder: b[2],
		}
	}

	ret.Result.Time = recv.Result.Time

	return ret, nil
}

type bookInterm struct {
	returnMeta
	Result struct {
		Bids [][]int
		Asks [][]int
		Time int64 `json:"t"`
	}
	Time int64
}

//BookJSON response struct for GetBook
type BookJSON struct {
	returnMeta
	Result struct {
		Bids []Offer
		Asks []Offer
		Time int64 `json:"t"`
	}
}

//Offer struct for bid/ask elements
type Offer struct {
	Price    int
	Quantity int
	NumOrder int
}
