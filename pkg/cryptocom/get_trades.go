package cryptocom

import "fmt"

//GetTrades "Fetches the public trades for a particular instrument"
func GetTrades(instrumentName string) (TradesJSON, error) {
	method := "public/get-trades"
	params := fmt.Sprintf("instrument_name=%v", instrumentName)
	var ret TradesJSON
	if err := apiGetAndStore(baseURL+method+"?"+params, ret); err != nil {
		return TradesJSON{}, err
	}
	return ret, nil
}

//GetTradesAll Fetches the public trades for all instrument
func GetTradesAll() (TradesAllJSON, error) {
	method := "public/get-trades"
	var ret TradesAllJSON
	if err := apiGetAndStore(baseURL+method, ret); err != nil {
		return TradesAllJSON{}, err
	}
	return ret, nil
}

//TradesJSON response struct for GetTrades
type TradesJSON struct {
	returnMeta
	Result struct {
		InstrumentName string `json:"instrument_name"`
		Data           []Trade
	}
}

//TradesAllJSON response struct for GetTradesAll
type TradesAllJSON struct {
	returnMeta
	Result struct {
		Data []TradeWithInst
	}
}

//Trade struct
type Trade struct {
	DataTime uint64
	Side     string  `json:"s"`
	Price    float64 `json:"p"`
	Quantity float64 `json:"q"`
	TradeID  int64   `json:"d"`
	Time     int64   `json:"t"`
}

//TradeWithInst struct
type TradeWithInst struct {
	InstrumentName string `json:"i"`
	DataTime       uint64
	Side           string  `json:"s"`
	Price          float64 `json:"p"`
	Quantity       float64 `json:"q"`
	TradeID        int64   `json:"d"`
	Time           int64   `json:"t"`
}
