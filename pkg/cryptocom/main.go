package cryptocom

import "os"

var baseURL string
var apiKey, apiSecret string

func init() {
	baseURL = "https://api.crypto.com/v2/"
	apiKey = os.Getenv("CRYPTOCOM_API_KEY")
	apiSecret = os.Getenv("CRYPTOCOM_API_SECRET")
}

type returnMeta struct {
	ID     int
	Method string
	Code   int
}
