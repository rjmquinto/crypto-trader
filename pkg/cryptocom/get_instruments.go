package cryptocom

//GetInstruments "Provides information on all supported instruments (e.g. BTC_USDT)"
func GetInstruments() (InstrumentsJSON, error) {
	method := "public/get-instruments"
	var ret InstrumentsJSON
	if err := apiGetAndStore(baseURL+method, &ret); err != nil {
		return InstrumentsJSON{}, err
	}

	return ret, nil
}

//InstrumentsJSON is the response struct for GetInstruments
type InstrumentsJSON struct {
	returnMeta
	Result struct {
		Instruments []Instrument
	}
}

//Instrument is the struct for each individual instrument
type Instrument struct {
	InstrumentName       string `json:"instrument_name"`
	QuoteCurrency        string `json:"quote_currency"`
	BaseCurrency         string `json:"base_currency"`
	PriceDecimals        int    `json:"price_decimals"`
	QuantityDecimals     int    `json:"quantity_decimals"`
	MarginTradingEnabled bool   `json:"margin_trading_enabled"`
}
