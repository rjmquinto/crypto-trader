package cryptocom

import "fmt"

//GetTicker "Fetches the public tickers for an instrument (e.g. BTC_USDT)."
func GetTicker(instrumentName string) (TickerJSON, error) {
	method := "public/get-ticker"
	params := fmt.Sprintf("instrument_name=%v", instrumentName)
	var ret TickerJSON
	if err := apiGetAndStore(baseURL+method+"?"+params, ret); err != nil {
		return TickerJSON{}, err
	}
	return ret, nil
}

//GetTickerAll "Fetches the public tickers for an instrument (e.g. BTC_USDT)."
func GetTickerAll() (TickerAllJSON, error) {
	method := "public/get-ticker"
	var ret TickerAllJSON
	if err := apiGetAndStore(baseURL+method, ret); err != nil {
		return TickerAllJSON{}, err
	}
	return ret, nil
}

//TickerJSON response struct for GetTicker
type TickerJSON struct {
	returnMeta
	Result struct {
		Data Ticker
	}
}

//TickerAllJSON response struct for GetTickerAll
type TickerAllJSON struct {
	returnMeta
	Result struct {
		Data []Ticker
	}
}

//Ticker struct
type Ticker struct {
	InstrumentName string  `json:"i"`
	BestBid        float64 `json:"b"`
	BestAsk        float64 `json:"k"`
	Latest         float64 `json:"a"`
	Time           int64   `json:"t"`
	Volume24H      float64 `json:"v"`
	Highest24H     float64 `json:"h"`
	Lowest24H      float64 `json:"l"`
	Change24H      float64 `json:"c"`
}
