package cryptocom

//CreateOrder "Creates a new BUY or SELL order on the Exchange."
func CreateOrder(query OrderParamsJSON) (OrderJSON, error) {

	postBody := map[string]interface{}{
		"id":     1,
		"method": "private/create-order",
	}

	params := map[string]interface{}{
		"instrument_name": query.InstrumentName,
		"side":            query.Side,
		"type":            query.Type,
	}

	if query.Type == LimitOrder || query.Type == StopLimitOrder {
		params["price"] = query.Price
	}

	if query.Type == LimitOrder || (query.Side == SellOrder && query.Type == MarketOrder) || (query.Side == SellOrder && query.Type == StopLossOrder) {
		params["quantity"] = query.Quantity
	}

	if (query.Side == BuyOrder && query.Type == MarketOrder) || (query.Side == BuyOrder && query.Type == StopLossOrder) {
		params["notional"] = query.Notional
	}

	if query.ClientOID != "" {
		params["client_oid"] = query.ClientOID
	}

	if query.TimeInForce != "" {
		params["time_in_force"] = query.TimeInForce
	}

	if query.ExecInst != "" {
		params["exec_inst"] = query.ExecInst
	}

	if query.Type == StopLossOrder || query.Type == StopLimitOrder || query.Type == TakeProfitOrder || query.Type == TakeProfitLimitOrder {
		params["trigger_price"] = query.TriggerPrice
	}

	postBody["params"] = params

	authorize(postBody)

	var ret OrderJSON
	if err := apiPostAndStore(baseURL+postBody["method"].(string), postBody, &ret); err != nil {
		return OrderJSON{}, nil
	}

	return ret, nil
}

//OrderParamsJSON holds the query for CreateOrder
type OrderParamsJSON struct {
	InstrumentName string
	Side           OrderSide
	Type           OrderType
	Price          float64
	Quantity       float64
	Notional       float64
	ClientOID      string
	TimeInForce    OrderTimeInForce
	ExecInst       OrderExec
	TriggerPrice   float64
}

//OrderJSON response struct for CreateOrder
type OrderJSON struct {
	returnMeta
	Result Order
}

//Order struct for order receipts
type Order struct {
	OrderID   string
	ClientOID string
}

//OrderSide enum
type OrderSide string

const (
	//BuyOrder side
	BuyOrder OrderSide = "BUY"
	//SellOrder side
	SellOrder OrderSide = "SELL"
)

//OrderType enum
type OrderType string

const (
	//LimitOrder order type
	LimitOrder OrderType = "LIMIT"

	//MarketOrder order type
	MarketOrder OrderType = "MARKET"

	//StopLossOrder order type
	StopLossOrder OrderType = "STOP_LOSS"

	//StopLimitOrder order type
	StopLimitOrder OrderType = "STOP_LIMIT"

	//TakeProfitOrder order type
	TakeProfitOrder OrderType = "TAKE_PROFIT"

	//TakeProfitLimitOrder order type
	TakeProfitLimitOrder OrderType = "TAKE_PROFIT_LIMIT"
)

//OrderTimeInForce enum
type OrderTimeInForce string

const (
	//GoodTillCancel is the default time-in-force
	GoodTillCancel OrderTimeInForce = "GOOD_TILL_CANCEL"

	//FillOrKill fills the order completely and immediately or not at all
	FillOrKill OrderTimeInForce = "FILL_OR_KILL"

	//ImmediateOrCancel fills the order immediately but possibly not completely
	ImmediateOrCancel OrderTimeInForce = "IMMEDIATE_OR_CANCEL"
)

//OrderExec enum
type OrderExec string

//PostOnly for post to book prior to execution
const PostOnly OrderExec = "POST_ONLY"
