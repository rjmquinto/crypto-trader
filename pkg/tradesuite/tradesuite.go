package tradesuite

import "gitlab.com/rjmquinto/crypto-trader/pkg/cryptocom"

//TradeSuite is the interface for trading modules
//It's designed so that you can replace it with a dummy
type TradeSuite interface {
	CheckIfContinue() bool
	GetPendingOrders() ([]cryptocom.Order, error)
	UpdateOrderStatus(cryptocom.Order)
	GetPositions() (map[string]Position, error)
	GetInstruments() ([]cryptocom.Instrument, error)
	IsBuySignal(instrumentName string) (Signal, error)
	IsSellSignal(instrumentName string) (Signal, error)
	GetBuyScore(instrumentName string, params map[string]interface{}) (int, error)
	GetBudget() (int, error)
	PutSellOrder(instrumentName string, params map[string]interface{}) error
	PutBuyOrder(instrumentName string, params map[string]interface{}) error
	WaitTillNextPeriod() error
}

//Position struct for states of positions held
type Position struct {
	Currency     string
	Instrument   cryptocom.Instrument
	Quantity     float64
	AveragePrice float64
	Time         int64
}

//Signal struct for buy and sell signals and additional parameters
type Signal struct {
	Valid  bool
	Params map[string]interface{}
}
