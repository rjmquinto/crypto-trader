package tradesuite

import (
	"log"
	"sort"
	"time"

	"gitlab.com/rjmquinto/crypto-trader/pkg/cryptocom"
)

//Run executes the trading suite provided
func Run(ts TradeSuite) {
	for ts.CheckIfContinue() {
		orders, err := ts.GetPendingOrders()
		if err != nil {
			reportErrorAndSleep(err)
			continue
		}

		for _, order := range orders {
			ts.UpdateOrderStatus(order)
		}

		positions, err := ts.GetPositions()
		if err != nil {
			reportErrorAndSleep(err)
			continue
		}

		instruments, err := ts.GetInstruments()
		if err != nil {
			reportErrorAndSleep(err)
			continue
		}

		potentialBuys := make([]tradeScore, 0, 25)

		shouldContinue := false
		for _, inst := range instruments {
			if _, exists := positions[inst.InstrumentName]; exists {
				signal, err := ts.IsSellSignal(inst.InstrumentName)
				if err != nil {
					reportErrorAndSleep(err)
					shouldContinue = true
					break
				}

				if signal.Valid {
					if err := ts.PutSellOrder(inst.InstrumentName, signal.Params); err != nil {
						reportErrorAndSleep(err)
						shouldContinue = true
						break
					}
				}
			} else {
				signal, err := ts.IsBuySignal(inst.InstrumentName)
				if err != nil {
					reportErrorAndSleep(err)
					shouldContinue = true
					break
				}

				if signal.Valid {
					score, err := ts.GetBuyScore(inst.InstrumentName, signal.Params)
					if err != nil {
						reportErrorAndSleep(err)
						shouldContinue = true
						break
					}

					potentialBuys = append(potentialBuys, tradeScore{instrument: inst, params: signal.Params, score: score})
				}
			}
		}

		if shouldContinue {
			continue
		}

		budget, err := ts.GetBudget()
		if err != nil {
			reportErrorAndSleep(err)
			continue
		}

		sort.Slice(potentialBuys, func(i, j int) bool {
			return potentialBuys[i].score > potentialBuys[j].score
		})

		for i := 0; i < budget && i < len(potentialBuys); i++ {
			if err := ts.PutBuyOrder(potentialBuys[i].instrument.InstrumentName, potentialBuys[i].params); err != nil {
				reportErrorAndSleep(err)
				break
			}
		}
	}
}

func reportErrorAndSleep(err error) {
	log.Println(err)
	time.Sleep(time.Second * 10)
}

type tradeScore struct {
	instrument cryptocom.Instrument
	params     map[string]interface{}
	score      int
}
