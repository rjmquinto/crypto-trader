package indicators

//MACD returns the MACD and signal values
func MACD(data []float64, fastLength int, slowLength int, signalLength int) ([]float64, []float64, error) {
	if fastLength == 0 {
		fastLength = 12
	}
	if slowLength == 0 {
		slowLength = 26
	}
	if signalLength == 0 {
		signalLength = 9
	}

	fastEMA, err := ExponentialMovingAverage(data, fastLength, 0)
	if err != nil {
		return nil, nil, err
	}

	slowEMA, err := ExponentialMovingAverage(data, slowLength, 0)
	if err != nil {
		return nil, nil, err
	}

	macd := make([]float64, len(slowEMA))
	for i := range slowEMA {
		macd[len(macd)-i-1] = fastEMA[len(fastEMA)-i-1] - slowEMA[len(slowEMA)-i-1]
	}

	signal, err := ExponentialMovingAverage(macd, signalLength, 0)
	if err != nil {
		return nil, nil, err
	}

	return macd, signal, nil
}
