package indicators

import "fmt"

//ExponentialMovingAverage returns the exponential moving average over the length and smoothing factor
//If smooth is set at 0, the default value of 2 will be used.
func ExponentialMovingAverage(data []float64, length int, smooth float64) ([]float64, error) {
	if length <= 0 {
		return nil, fmt.Errorf("length <= 0")
	}

	if length >= len(data) {
		return nil, fmt.Errorf("length >= length of data")
	}

	if smooth == 0.00 {
		smooth = 2.00
	}

	currentSum := 0.00
	for i := 0; i < length; i++ {
		currentSum = currentSum + data[i]
	}

	k := smooth / (float64(length + 1))

	ret := make([]float64, 0, len(data)-length+1)
	ret = append(ret, currentSum/float64(length))

	for i := length; i < len(data); i++ {
		ret = append(ret, data[i]*k+ret[len(ret)-1]*(1-k))
	}

	return ret, nil
}
