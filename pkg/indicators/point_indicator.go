package indicators

import "gitlab.com/rjmquinto/crypto-trader/pkg/candlesdb"

//ClosePI returns the candlestick close value
func ClosePI(data candlesdb.Candle) float64 {
	return data.Close
}

//HighLowPI returns the candlestick median value
func HighLowPI(data candlesdb.Candle) float64 {
	return (data.High + data.Low) / 2.00
}

//HighLowClosePI returns the average of the high, low, and closing value
func HighLowClosePI(data candlesdb.Candle) float64 {
	return (data.High + data.Low + data.Close) / 3.00
}
