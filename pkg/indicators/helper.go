package indicators

import (
	"gitlab.com/rjmquinto/crypto-trader/pkg/candlesdb"
)

//CandlesToArray returns an array of
func CandlesToArray(candles []candlesdb.Candle, source func(candlesdb.Candle) float64) []float64 {
	vals := make([]float64, len(candles))
	for i, v := range candles {
		vals[i] = source(v)
	}
	return vals
}
