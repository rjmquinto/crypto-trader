package indicators

import (
	"fmt"
)

//MovingAverage returns the moving average over the length
func MovingAverage(data []float64, length int) ([]float64, error) {
	if length <= 0 {
		return nil, fmt.Errorf("length <= 0")
	}

	if length > len(data) {
		return nil, fmt.Errorf("length > length of data")
	}

	currentSum := 0.00
	for i := 0; i < length; i++ {
		currentSum = currentSum + data[i]
	}

	ret := make([]float64, 0, len(data)-length+1)
	ret = append(ret, currentSum/float64(length))

	for i := length; i < len(data); i++ {
		currentSum = currentSum - data[i-length] + data[i]
		ret = append(ret, currentSum/float64(length))
	}

	return ret, nil
}
