module gitlab.com/rjmquinto/crypto-trader

go 1.15

require (
	gopkg.in/yaml.v2 v2.2.8 // indirect
	gorm.io/driver/postgres v1.0.8
	gorm.io/gorm v1.20.12
)
